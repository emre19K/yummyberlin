# Business Finder App

This is a business search app created using React.js. It allows users to search for local businesses based on keywords, such as "pasta" or "coffee." The app will display the businesses in a list format, with details such as the business name, address, and phone number.

## Installation

To install the necessary dependencies, run the following command in the root directory of the project:
```
npm install
cd backend
npm install
```
This command will install all the dependencies required to run the app.

## Running the App

To start the development server, run the following command in the root directory:
```
npm start
```
This command will start the development server and open the app in your default web browser. You can then search for local businesses by entering keywords in the search bar.

## Usage

Once the app is running, the user can simply type in a keyword of a business they are looking for and the app will display a list of local businesses that match the keyword. The user can also filter the results by location, rating, or other available options.

## Note

Please ensure that you have Node and npm installed on your machine before running the above commands. If you are not sure whether you have Node and npm installed, you can check by running the following commands in your terminal:
```
node -v
```
```
npm -v
```
## Contribution

We welcome any contributions to improve the app. Feel free to submit pull requests. If you are interested in contributing to the project, please follow these steps:

1. Fork the repository
2. Create a new branch for your feature
3. Make your changes
4. Test your changes
5. Submit a pull request

We will review your changes and merge them into the master branch if they align with the project's goals and coding standards.

## Issues

If you encounter any issues while using the app, please feel free to open an issue in the repository. Our team will do our best to resolve the issue as soon as possible.

## Codebase

https://gitlab.com/emre19K/yummyberlin

