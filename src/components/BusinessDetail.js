import React from "react";
/*
import { useNavigate } from "react-router-dom";
import { SiGooglemaps } from "react-icons/si";
import { MdOutlineArrowBackIos } from "react-icons/md";*/
import { SiGooglemaps } from "react-icons/si";
import Card from 'react-bootstrap/Card';
import ListGroup from 'react-bootstrap/ListGroup';

const styles = {
  card: {
    'textAlign': 'center'
  },
  img: {
    'height': '430px',
    'width': '650px',
    'borderRadius': '30px',
  },
  title: {
    'fontSize': '100px'
  },
  icon: {
    'cursor': 'pointer'
  }
}

const BusinessDetail = ({ business }) => {
  // const nav = useNavigate();
  if (!business) return null;

  const { name, is_closed, location, phone, image_url } = business;

  const showInMapClicked = () => {
    window.open("https://maps.google.com?q=" + name + " " + location.address1);
  };
  /*
  const backHomeClicked = () => {
    nav("/");
     };
*/
  return (
    <Card style={styles.card}>
      <Card.Img style={styles.img} variant="top" src={image_url} />
      <Card.Body>
        <Card.Title style={styles.title}>{name}</Card.Title>
      </Card.Body>
      <ListGroup className="list-group-flush">
        <ListGroup.Item>{location.address1}</ListGroup.Item>
        <ListGroup.Item>{phone}</ListGroup.Item>
        <ListGroup.Item>{is_closed ? <p>Geschlossen</p> : <p>Geöffnet</p>}</ListGroup.Item>
      </ListGroup>
      <Card.Body>
        <Card.Subtitle><SiGooglemaps style={styles.icon} size={80} onClick={showInMapClicked} /></Card.Subtitle>
      </Card.Body>
    </Card>
  );
};

export default BusinessDetail;
