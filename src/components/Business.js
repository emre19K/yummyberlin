import React from "react";

const styles = {
  business: {
    borderWidth: "2px",
    borderColor: "red",
  },
  img: {
    width: "300px",
    height: "200px",
  },
  header: {
    fontSize: "17px",
    marginLeft: "5px",
  },
  infoDiv: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
  },
  rating: {
    marginLeft: "9px",
  },
  pricing: {
    marginLeft: "5px",
  },
};

const Business = ({ business }) => {
  return (
    <>
      <div style={styles.business}>
        {business ? (
          business.image_url ? (
            <img style={styles.img} src={business.image_url} />
          ) : null
        ) : null}
        <h1 style={styles.header}>{business.name}</h1>
        <div style={styles.infoDiv}>
          <p style={styles.rating}>
            {business.rating} ({business.review_count})
          </p>
          <p style={styles.pricing}>{business.price}</p>
        </div>
      </div>
    </>
  );
};

export default Business;
