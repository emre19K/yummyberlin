import React from "react";
import { Link } from "react-router-dom";

import Business from "./Business";

const styles = {
  businesses: {
    display: "flex",
    flexDirection: "row",
  },
  business: {
    marginRight: "12px",
  },
};

const ResultsList = ({ title, results }) => {
  if (!results || !results.length) return null;

  return (
    <>
      <h2>{title}</h2>
      <div style={styles.businesses}>
        {results.map((b) => (
          <div style={styles.business} key={b.id}>
            <Link to={`/detail/${b.id}`} state={b}>
              <Business business={b} />
            </Link>
          </div>
        ))}
      </div>
      <hr />
    </>
  );
};

export default ResultsList;
