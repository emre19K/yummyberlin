import { useEffect, useState } from "react";
import api from "../api/api";

const useResults = () => {
  const [results, setResults] = useState([]);
  const [errorMessage, setErrorMessage] = useState("");
  const [isLoading, setIsLoading] = useState(true);

  const searchApi = async (searchTerm) => {
    setIsLoading(true);
    setResults([]);
    try {
      const res = await api.get("/api", {
        params: {
          limit: 50,
          term: searchTerm,
          location: "berlin",
        },
      });
      setResults(Object.values(res.data));
      setErrorMessage(null);
      setIsLoading(false);
    } catch (e) {
      setErrorMessage("Etwas ist schief gelaufen...");
    }
  };

  useEffect(() => {
    searchApi("Steak");
  }, []);

  return [searchApi, results, errorMessage, isLoading];
};

export default useResults;
