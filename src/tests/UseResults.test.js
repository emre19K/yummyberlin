import { act, renderHook } from "@testing-library/react";
import api from "../api/api";
import useResults from "../hooks/useResults";

jest.mock("../api/api");

describe("useResults", () => {
    test("default search term is 'Steak'", async () => {
        api.get.mockImplementation(() =>
            Promise.resolve({
                data: [{ name: "Test Business 1" }, { name: "Test Business 2" }],
            })
        );

        const { result } = renderHook(() => useResults());

        await act(async () => {
            await Promise.resolve();
        });

        expect(api.get).toHaveBeenCalledWith("/api", {
            params: { limit: 50, term: "Steak", location: "berlin" },
        });
        expect(result.current[1]).toEqual([
            { name: "Test Business 1" },
            { name: "Test Business 2" },
        ]);
    });

    test("searchApi function updates results", async () => {
        api.get.mockImplementation(() =>
            Promise.resolve({
                data: [{ name: "Test Business 3" }, { name: "Test Business 4" }],
            })
        );

        const { result } = renderHook(() => useResults());

        await act(async () => {
            result.current[0]("pizza");
            await Promise.resolve();
        });

        expect(api.get).toHaveBeenCalledWith("/api", {
            params: { limit: 50, term: "pizza", location: "berlin" },
        });

        expect(result.current[1]).toEqual([
            { name: "Test Business 3" },
            { name: "Test Business 4" },
        ]);
    });

    test("searchApi function sets error message on failed request", async () => {
        api.get.mockImplementation(() =>
            Promise.reject({
                message: "Request failed",
            })
        );

        const { result } = renderHook(() => useResults());

        await act(async () => {
            result.current[0]("pizza");
            await Promise.resolve();
        });

        expect(result.current[2]).toEqual("Etwas ist schief gelaufen...");
    });

    test("searchApi function sets isLoading state to true before request and false after", async () => {
        api.get.mockImplementation(() =>
            Promise.resolve({
                data: [{ name: "Test Business 3" }, { name: "Test Business 4" }],
            })
        );
        const { result } = renderHook(() => useResults());

        expect(result.current[3]).toBe(true);

        await act(async () => {
            result.current[0]("pizza");
            await Promise.resolve();
        });

        expect(result.current[3]).toBe(false);
    });
});

