import React from "react";
import { render } from "@testing-library/react";
import ResultsList from "../components/ResultsList";
import { MemoryRouter } from "react-router-dom";

const results = [
  { id: "1", name: "Business 1" },
  { id: "2", name: "Business 2" },
  { id: "3", name: "Business 3" },
];

describe("ResultsList", () => {
  test("renders correctly with results", () => {
    const { getByText } = render(
      <MemoryRouter>
        <ResultsList title="Test Title" results={results} />
      </MemoryRouter>
    );

    expect(getByText("Test Title")).toBeInTheDocument();
    results.forEach((result) => {
      expect(getByText(result.name)).toBeInTheDocument();
    });
  });

  test("renders null if no results prop is passed", () => {
    const { container } = render(
      <MemoryRouter>
        <ResultsList title="Test Title" />
      </MemoryRouter>
    );

    expect(container.firstChild).toBeNull();
  });
});