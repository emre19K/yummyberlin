import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import React from "react";

import IndexScreen from "./screens/IndexScreen";
import DetailScreen from "./screens/DetailScreen";

function App() {
  return (
    <>
      <Router>
        <Routes>
          <Route excact path="/detail/:id" element={<DetailScreen />} />
          <Route excact path="/" element={<IndexScreen />} />
        </Routes>
      </Router>
    </>
  );
}

export default App;
