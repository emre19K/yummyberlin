import { useLocation } from "react-router-dom";
import React from "react";

import BusinessDetail from "../components/BusinessDetail";

const DetailScreen = () => {
  const location = useLocation();

  return (
    <div>
      <BusinessDetail business={location.state} />
    </div>
  );
};

export default DetailScreen;
