import { useState } from "react";
import useResults from "../hooks/useResults";
import React from "react";

import ResultsList from "../components/ResultsList";

const IndexScreen = () => {

  const [term, setTerm] = useState("");
  const [searchApi, results, errorMessage, isLoading] = useResults();

  const handleSubmit = (term) => {
    searchApi(term);
  };

  const filterByPricing = (price) => {
    return results.filter((result) => {
      return result.price === price;
    });
  };

  return (
    <>
      <h1>YummyBerlin</h1>
      <div>
        <input
          size={25}
          placeholder="Suchen..."
          onChange={(e) => setTerm(e.target.value)}
        />
        <button onClick={() => handleSubmit(term)}>Suchen</button>
      </div>
      {isLoading ? <p>Bitte warten...</p> : null}
      {errorMessage ? <p>{errorMessage}</p> : null}
      {results ? (
        <div>
          <ResultsList title="Preisgünstig" results={filterByPricing("€")} />
          <ResultsList title="Preisleistung" results={filterByPricing("€€")} />
          <ResultsList title="Geschmack" results={filterByPricing("€€€")} />
          <ResultsList title="Titan" results={filterByPricing("€€€€")} />
        </div>
      ) : null}
    </>
  );
};

export default IndexScreen;
