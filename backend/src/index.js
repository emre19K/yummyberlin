const express = require("express");
const app = express();
const cors = require("cors");
const yelp = require("yelp-fusion");
const client = yelp.client(
  "VQMjO1XxuLc1TzfRHZ2XfEbKObeDTCnPJz4j8vjEGQpUzK8hoJT3ktoup7wKzNtGDRjp0gjDYC3ZNN1OcxPbD8pMadu19ub-3aflTAhNwf09Yk9ih-U0fr5uQ7nJY3Yx"
);
const port = 3001;

app.use(cors());

app.get("/api", (req, res) => {
  client
    .search({
      limit: req.query.limit,
      term: req.query.term,
      location: req.query.location,
    })
    .then((response) => {
      res.send(response.jsonBody.businesses);
    })
    .catch((e) => {
      res.send(e);
    });
});

const server = app.listen(port);

module.exports = server;
