const request = require("supertest");
const server = require("./src/index");


test("server is running on port 3001", () => {
    const address = server.address();
    expect(address.port).toBe(3001);
});

afterAll(() => {
    server.close();
});
